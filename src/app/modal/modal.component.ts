import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { IDropdownSettings, } from 'ng-multiselect-dropdown';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  myForm!: FormGroup<any> 
  dropdownList =<any>[];
  dropdownSettings:IDropdownSettings={};
  // closeResult!: string;
	constructor(private mod: NgbModal,
    private dt: DataService,
    private fb: FormBuilder) {}
  

    ngOnInit() {
      this.dropdownList = [
        { item_id: 1, skill: 'Html'},
        { item_id: 2, skill: 'css' },
        { item_id: 3, skill: 'Js' },
        { item_id: 4, skill: 'Angular' },
        { item_id: 5, skill: 'React' },
        { item_id: 6, skill: 'Java' }
      ];
      this.dropdownSettings = {
        idField: 'item_id',
        textField: 'skill',
        allowSearchFilter: true
      };
     this.myForm = this.fb.group({
        title: new FormControl(['']),
        org: new FormControl(['']),
        salary: new FormControl(['']),
        description: new FormControl(['']),
        skills: []

      });
    }
	openLg(content: any) {
		this.mod.open(content, { size: 'lg' });
	}
  onSubmit(form:FormGroup) {
    this.dt.jobData.push(this.myForm.value);
    this.myForm.reset();
  //  console.log(form.value);
  }

}
 
   
    
   