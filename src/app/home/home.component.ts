import { Component,OnInit } from '@angular/core';
import * as studentData from '../read.json';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
	model!: NgbDateStruct;
	data:any= (studentData as any).default;
	page = 1;
	pageSize = 4;
	collectionSize = this.data.length;

	constructor() {
		this.refreshCountries();
	}

	refreshCountries() {
		this.data = (studentData as any).default.map((data: any, i: number) => ({ id: i + 1, ...data})).slice(
			(this.page - 1) * this.pageSize,
			(this.page - 1) * this.pageSize + this.pageSize,
		);
	}
  ngOnInit(): void { }
}