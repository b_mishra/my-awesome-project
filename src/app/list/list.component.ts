import { Component,OnInit} from '@angular/core';
import { DataService} from '../data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'; 

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit{
  jobData:any;
  constructor(private data:DataService,private mod:NgbModal){
    this.refreshPage();
  }
 
  page = 1;
	pageSize = 4;
	collectionSize = this.data.jobData.length;
	refreshPage() {
		this.jobData = this.data.jobData.map((jobData: any, i: number) => ({ id:i + 1, ...jobData})).slice(
			(this.page - 1) * this.pageSize,
			(this.page - 1) * this.pageSize + this.pageSize,
		);
	}
	openLg(content: any) {
		this.mod.open(content, { size: 'lg' });
	}
  ngOnInit(): void {
    this.refreshPage;
  }
  showToasterSuccess(){
    this.data.showSuccess("successfully submited !!!")
 }

}
