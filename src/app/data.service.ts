import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
  export class DataService{

    constructor(private toastr: ToastrService){
    }
  jobData:any[] = [
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development',
        org: 'def',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development1213233',
        org: 'def',
        skills:[
          {skill: 'Html'},
        ],
        salary:'134',
        description: 'something comkdnfsdfdsfhgfdsadf',
      },
      {
        title: 'Software Development1213233',
        org: 'def',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'something comkdnfsdfdsfhgfdsadf',
      },
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
      {
        title: 'Software Development',
        org: 'abc',
        skills:[
          {skill: 'Html'},
          {skill: 'Java'},
          {skill: 'Sql'},
        ],
        salary:'134',
        description: 'Software developers write code using programming languages, build software components, and test their designs. As a part of software testing, developers address issues or errors. After deploying an application, software developers perform maintenance, updates, and upgrades as needed.',
      },
    ];
    showSuccess(message:string){
      this.toastr.success(message)
  }
  }
